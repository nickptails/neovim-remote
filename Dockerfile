ARG ALPINE_VERSION=3.16
ARG PYTHON_VERSION=3.10
ARG NVR_VERSION=2.5.1

FROM python:${PYTHON_VERSION}-alpine${ALPINE_VERSION} AS builder

ARG NVR_VERSION

RUN apk add --no-cache \
        build-base \
        linux-headers && \
    python3 -m pip install -U --no-cache-dir \
        pip \
        setuptools \
        wheel && \
    mkdir -p /wheelhouse && \
    python3 -m pip wheel -w /wheelhouse --no-cache-dir \
        neovim-remote==${NVR_VERSION}


FROM python:${PYTHON_VERSION}-alpine${ALPINE_VERSION}

ARG NVR_VERSION

COPY --from=builder /wheelhouse /wheelhouse
RUN apk add --no-cache \
        bash \
        nmap-ncat && \
    python3 -m pip install -U --no-cache-dir \
        pip \
        setuptools && \
    python3 -m pip install --no-index --find-links=/wheelhouse --no-cache-dir \
        neovim-remote==${NVR_VERSION}

CMD ["nvr"]
